"use client";
import { redirect, useRouter } from "next/navigation";
import React, { FormEvent, useState } from "react";
import axios from "axios";

function LoginPage() {
    const [formData, setFormData] = useState({
        username: "",
        password: "",
    });
    const router = useRouter();
    const loginHandler = async (e: FormEvent) => {
        e.preventDefault();
        const response = await axios.post("/api/", {
            body: formData,
        });

        if (response.data == "Login Success" && response.status == 200) {
            router.push("/home");
        } else {
            throw new Error("Invalid Credentials");
        }
    };

    const inputHandler = (e: any) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };
    return (
        <div className="flex justify-center items-center h-96">
            <form onSubmit={loginHandler} className="space-y-3">
                <div className="flex flex-col space-y-2">
                    <label htmlFor="username">Username</label>
                    <input
                        onChange={inputHandler}
                        name="username"
                        type="text"
                        placeholder="Username"
                        className="placeholder:text-gray-600 text-black px-2 w-full py-1"
                    />
                </div>
                <div className="flex flex-col space-y-2">
                    <label htmlFor="password">Password</label>
                    <input
                        onChange={inputHandler}
                        name="password"
                        type="password"
                        placeholder="**********"
                        className="placeholder:text-gray-600 text-black px-2 w-full py-1"
                    />
                </div>
                <button
                    className="px-8 py-2 text-black bg-[#7dfdfe]"
                    type="submit"
                >
                    Login
                </button>
            </form>
        </div>
    );
}

export default LoginPage;
