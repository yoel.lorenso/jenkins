import { NextResponse } from "next/server";
import { MongoClient } from "mongodb";

export async function POST(req: Request, res: NextResponse) {
    const data = await req.json();

    const { username, password } = data.body;

    let dbURI = process.env.DB_URI!!;

    const client = new MongoClient(dbURI);

    try {
        const database = await client.db("next-db");

        const users = await database.collection("users");

        // This is for inserting a username/password
        // const doc = {
        //     username,
        //     password,
        // };
        // const result = await users.insertOne(doc);
        const result = await users.findOne({
            username: username,
            password: password,
        });
        if (result == null) {
            throw new Error("Invalid Credentials");
        }
        return new Response("Login Success", { status: 200 });
    } catch (err) {
        return new Response(`Failed: ${JSON.stringify(err)}`, {
            status: 500,
        });
    } finally {
        await client.close();
    }
}
