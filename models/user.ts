import { Schema, model, models } from "mongoose";

const userSchema = new Schema({
    username: String,
    password: {
        type: String,
        required: true,
    },
});

const User = models.Test || model("User", userSchema);

export default User;
